import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../servicios/heroes.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  heroes:any[]=[];
  valor:any;

  constructor(
              private activatedRoyuter:ActivatedRoute,
              private _heroesService:HeroesService
              ) {  }

  ngOnInit() {
    this.activatedRoyuter.params.subscribe( params => {
      console.log( params );
      this.valor = params['heroeValue'];
      this.heroes = this._heroesService.buscarHeroe( params['heroeValue']);
      console.warn("------- llamada del servicio -----");
      console.log(this.heroes);

    }
  }

}
